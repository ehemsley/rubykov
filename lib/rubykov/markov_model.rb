module Rubykov
  class MarkovModel
    def initialize(order, training_data)
      raise ArgumentError unless order.is_a? Integer
      raise ArgumentError unless training_data.is_a? Array

      @order = order
      @representation = {}
      add_data_to_model(training_data)
    end

    def train(training_data)
      add_data_to_model(training_data)
    end

    def chain
      chain_enumerator
    end

    def chain_with_seed(seed_state)
      chain_enumerator(seed_state)
    end

    def states
      @representation.keys
    end

    def transitions
      @representation
    end

    private

    def add_data_to_model(training_data)
      training_data.each_cons(@order + 1).each do |datum|
        key = datum.first(@order)
        value = datum.last
        if @representation.include? key
          @representation[key] << value
        else
          @representation[key] = [value]
        end
      end
    end

    def chain_enumerator(seed_state = states.sample)
      Enumerator.new do |output|
        current_state = seed_state
        current_state.each do |word|
          output << word
        end

        loop do
          break if @representation[current_state].nil?
          next_word = @representation[current_state].sample
          output << next_word
          current_state = current_state.last(@order-1) + [next_word]
        end
      end
    end
  end
end
