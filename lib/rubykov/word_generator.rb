module Rubykov
  class WordGenerator < MarkovModel
    def initialize(order, training_data)
      super(order, training_data)
    end

    def train(training_data)
      add_data_to_model(training_data)
    end

    def word
      chain.to_a.join('').capitalize
    end

    private

    def massage_training_data(training_data)
      training_data.map { |datum| datum.downcase.split('') + ['\n'] }
    end

    def add_data_to_model(training_data)
      massage_training_data(training_data).each do |datum|
        super(datum)
      end
    end

    def chain_enumerator(seed_state = states.sample)
      Enumerator.new do |output|
        current_state = seed_state
        current_state.each do |word|
          output << word
        end

        loop do
          break if @representation[current_state].nil?
          next_letter = @representation[current_state].sample
          break if next_letter == '\\n'
          output << next_letter
          current_state = current_state.last(@order-1) + [next_letter]
        end
      end
    end
  end
end
