module Rubykov
  class TextGenerator < MarkovModel
    def initialize(order, training_text)
      super(order, massage_training_text(training_text))
    end

    def train(training_text)
      super(massage_training_text(training_text))
    end

    def character_limited_output(desired_length)
      length = 0
      words_to_sentences(chain.take_while { |word| length += (word.length + 1); length < desired_length } )
    end

    def word_limited_output(desired_length)
      words_to_sentences(chain.take(desired_length))
    end

    def sentence_limited_output(desired_length)
      length = 0
      output = []
      while length < desired_length
        output += chain.take_while do |word|
          length += 1 if is_sentence_finisher?(word)
          length < desired_length
        end
      end
      words_to_sentences(output)
    end

    private

    def words_to_sentences(words)
      massage_word_list(words).join(' ')
    end

    def massage_word_list(words)
      output = []
      words.each_with_index do |word, index|
        if word =~ /^[^\w\s]+$/
          output[-1] += word
        else
          output << word
        end
      end
      output
    end

    def is_sentence_finisher?(word)
      word =~ (/^[?!.]*$/)
    end

    def massage_training_text(training_text)
      training_text.downcase.gsub(/[^\w\s]+/) { " #{$&}" }.split(' ')
    end
  end
end
